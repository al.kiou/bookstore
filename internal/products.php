<table>
<tr>
	<th>Title</th>
	<th>Price</th>
</tr>

<?php
  require_once("dbconnect.php"); // include file with DBConnection class
  $db = new DBConnection(); // instance of DBConnection
  $db->connect(); // create connection to db server

  if ( !isset($_REQUEST['catid']) ){ // if no category is chosen
    $sqlquery = "SELECT id,title,price FROM product"; // select every product
  }else{
    $category = $_REQUEST['catid']; // get product id from GET request
    $sqlquery = "SELECT id,title,price FROM product WHERE category=$category"; // select products only from given category
  }

  $result = $db->select($sqlquery); // get result

  // print product titles and prices, add acnchors to itemselect page, and product ids
  while ($row = $result->fetch_assoc()) {
  	echo "<tr><td><a href='index.php?page=itemselect&pid=$row[id]'>".
  			"$row[title]</a></td>".
  	      "<td>$row[price]</td></tr>";
  }
  $db->disconnect(); // disconnect from the db server
  unset($db); // unset variable (may be unnecessary)
?>
</table>
