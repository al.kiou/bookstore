<?php
  require_once("dbconnect.php"); // include file with DBConnection class
  $db = new DBConnection(); // instance of DBConnection
  $db->connect(); // create connection to db server

  $sqlquery = "SELECT id,name FROM category order by Name"; // create query
  $result = $db->select($sqlquery); // get result from executing query

  if ($result->num_rows > 0) { // if the result is not empty
      // output data of each row
      while($row = $result->fetch_assoc()) {
        echo '<li class="nav-item">';
        echo "<a class='nav-link' href='index.php?page=products&catid=$row[id]'>";
        echo "<i class='fas fa-fw fa-folder'></i>";
        echo "<span> $row[name] </span></a></li>";
      }
  } else {
      echo "0 results";
  }
  
  $db->disconnect(); // disconnect from db server
  unset($db); // unset the db variable (may be unnecessary)
?>
