

<?php
class DBConnection{

  private $user='root';
  private $pass='';
  private $host='localhost';
  private $db = 'bookstoredb';
  private $connection;
  private $statement;

  function __construct() {
  }

  function __destruct() {
  }

  function setCredentials($host, $user, $pass, $db){
    $this->host = $host;
    $this->user = $user;
    $this->pass = $pass;
    $this->db = $db;
  }

  // create connection (stored at $this->conn)
  function connect(){
    // Create connection
    $this->connection = new mysqli($this->host, $this->user, $this->pass, $this->db);
    // Check connection
    if ($this->connection->connect_error) {
        die("Connection failed: " . $this->connection->connect_error);
    }
  }

  function getConnection(){
    return $this->connection;
  }

  // close the connection
  function disconnect() {
    $this->connection->close();
  }

  // execute query
  function execute($sqlquery){
    if ($this->connection->query($sqlquery) === TRUE) {
      return true;
    } else {
        return "Error: " . $sqlquery . "<br>" . $this->connection->error;
    }
  }

  // return result of select query
  function select($sqlquery){
    $result = $this->connection->query($sqlquery);
    return $result;
  }

  // return the max value of thr column - table specified
  function selectMax($column, $table){
    $sqlquery = "SELECT MAX($column) FROM $table";
    $result = $this->connection->query($sqlquery);
    $row = $result->fetch_assoc();
    $s = "MAX($column)";
    return $row[$s];
  }

  function prepare($sqlquery){
    $this->connection -> prepare($sqlquery);
  }

  function bind_param($types, $var){
    $this->statement->bind_param($types, $var);
  }

  function sexecute(){
    $this->statement->execute();
  }

  function get_result(){
    return $this->statement->get_result();
  }

  // UNDER CONSTRUCTION
  // echo in table form the results of the search
  function printSelect($sqlquery){

  }

  // return true when correct credentials are given
  function checkLogin($username, $password){
    // construct sql query and get result from DB
    $sql = "SELECT passwd FROM customer WHERE uname=\"" .$username ."\"";
    $result = $this->connection->query($sql);

    if ($result->num_rows == 0){ // check if there is no row returned (username does not exits)
      print "Username doesn't exist.";
      return false;
    }else{
      $row = $result->fetch_assoc(); // get row as associative array
      if ($row['passwd'] == $password){ // check for password match
        return true;
      }// if
      return false;
    }// if-else
  }// checkLogin

}// Class

 ?>
