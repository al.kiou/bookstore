<?php
  if($_SESSION['username'] == '?'){
    echo 'You need to login before ordering a book';
  }else{
    require_once("dbconnect.php"); // include file with DBConnection class
    $db = new DBConnection(); // instance of DBConnection
    $db->connect(); // create connection to db server
    $pid = $_REQUEST['pid']; // get product id from GET request
    $sqlquery = "SELECT title,price,description FROM product WHERE id=$pid"; // create query
    $result = $db->select($sqlquery); // get result from executing query

    $row = $result->fetch_assoc(); // fetch result to associative array

    // echo product's details
    echo "<h3>Title: $row[title]</h3>";
    echo "<br>";
    echo "<h4>Price: $row[price] €</h4>";
    echo "<p>Description: $row[description]</p>";
    $img = "images/product/" .$pid .".jpg";
    echo "<img src='$img'>";

    // create form that posts to index:
    echo "<form method='post' action='index.php'>";
    echo "<input name='page' value='addtocart' type='hidden'/>"; // page: addtocart
    print "<input type='number' name='quantity' value='3'/>"; // quantity
    print "<input type='hidden' name='pid' value='" .$pid ."'/>"; // product id
    print "<input type='submit' name='submit' value='Προσθήκη στο καλάθι'>";
  }
?>
