<?php
  session_destroy(); // unset all session variables
  header("Location: index.php"); // head back to start page
?>
