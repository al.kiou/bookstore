<?php



  require_once("dbconnect.php"); // include file with DBConnection class
  $db = new DBConnection(); // instance of DBConnection
  $db->connect(); // create connection to db server

  // Update orders table
  $u = $_SESSION['username'];
  $sqlquery = "SELECT ID FROM customer WHERE uname='$u'"; // query to get ID from username
  $result = $db->select($sqlquery); // get result
  $row = $result->fetch_assoc(); // get associative array
  $uid = $row['ID']; // get ID value
  $sqlquery = "INSERT INTO  orders (Customer, oDate) VALUES ($uid,now())"; // query to insert new order
  echo $db->execute($sqlquery);

  // Update orderdetails table
  $oid = $db->selectMax("id", "orders");
  foreach($_SESSION['cart'] as $product => $quantity) {
    $sqlquery = "INSERT INTO  orderdetails (Orders, Quantity, Product) VALUES ($oid,$quantity,$product)"; // query to insert new order
    echo $db ->execute($sqlquery);
  }


  $db->disconnect(); // disconnect from db server
  unset($db); // unset the db variable (may be unnecessary)

  echo "<h3>Η Αγορά σας πραγματοποιήθηκε!<h3>";
  unset($_SESSION['cart']); // delete cart
?>
