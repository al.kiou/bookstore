<?php
  require_once("dbconnect.php"); // include file with DBConnection class
  $db = new DBConnection(); // instance of DBConnection
  $db->connect(); // create connection to db server

  echo $db->execute('ALTER TABLE product ADD picture varchar(255)');

  $db->disconnect(); // disconnect from db server
  unset($db); // unset the db variable (may be unnecessary)
?>
