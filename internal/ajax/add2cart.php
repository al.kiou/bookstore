<script>

var xmlhttp;
function ajaxGet() {
  if (window.XMLHttpRequest) {
    // for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  }
  else {
    // for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = showresponse;
  var a = document.getElementById('field1').value;
  var b = document.getElementById('field2').value;
  xmlhttp.open("GET","ajax_get.php?a="+a+"&b="+b,true);
  xmlhttp.send();
}


function ajaxPost() {
  if (window.XMLHttpRequest) {
    // for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  }else {
    // for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }

  xmlhttp.onreadystatechange = showresponse;
  xmlhttp.open("POST","internal/addtocart.php",true);
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var pid = document.getElementById('pid').value;
  var quantity = document.getElementById('quantity').value;
  xmlhttp.send("a="+a+"&b="+b);

}


function showresponse() {
      if (xmlhttp.readyState==4 && xmlhttp.status==200) {
             document.getElementById("showResponse").innerHTML = xmlhttp.responseText;
      }
}

</script>

<div>
  <?php
  // Get max ID value for product ID
  require_once("internal/dbconnect.php"); // include file with DBConnection class
  $db = new DBConnection(); // instance of DBConnection
  $db->connect(); // create connection to db server

  $maxID = $db->selectMax("ID", "product");

  $db->disconnect(); // disconnect from db server
  unset($db); // unset the db variable (may be unnecessary)
  ?>

  Product ID: <input type='number' id='pid' value='1' min='1' max='<?php echo $maxID;?>'/>
  <br>
  Quantity: <input type='number' id='quantity' value='3'/>
  <br>
  <input type="button" onclick="ajaxPost()" name="button" value="Add to Cart">


</div>
