<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Bookstore</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<?php
session_start();  // start session
  // if there is no logged in user, set username as '?'
  if( ! isset($_SESSION['username'])) {
  	$_SESSION['username']='?';
  }
  // set start page as default
  if( ! isset($_REQUEST['page'])) {
    $_REQUEST['page']='start';
  }
?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Logo -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Book Store</div>
      </a> <!-- Sidebar - Logo -->

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Sidebar - Username -->
      <li class="nav-item">
        <a class="nav-link" href="?page=ajax/add2cart">
          <i class="fas fa-fw "></i>
          <span>Ajax Page</span></a>
      </li><!-- Sidebar - Username -->

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Sidebar - Username -->
      <li class="nav-item">
        <a class="nav-link" href="?page=profilesettings">
          <i class="fas fa-fw "></i>
          <span>User: <?php echo $_SESSION['username'];?></span></a>
      </li><!-- Sidebar - Username -->

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <?php
      $page = $_REQUEST['page'];
      // if a user is logged in, show the product categories
      $filename = "internal/$page" ."side.php";
      if((@include $filename) === false)
      {
          echo "No additional options";
      }
      ?>

    </ul> <!-- Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">



      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a class="nav-link" href="index.php?page=start">Αρχικη</a></li>
            <li class="nav-item"><a class="nav-link" href="?page=shopinfo">Κατάστημα</a></li>
            <li class="nav-item"><a class="nav-link" href="?page=products">Προϊόντα</a></li>
          	<li class="nav-item"><a class="nav-link" href="?page=contact">Επικοινωνία</a></li>
          	<li class="nav-item"><a class="nav-link" href="?page=cart">Καλάθι Αγορών</a></li>
            <?php if($_SESSION['username'] == '?'){ // if no user is connected
              echo '<li class="nav-item"><a class="nav-link" href="?page=login">Σύνδεση</a></li>'; // add login option
            }else{
              echo '<li class="nav-item"><a class="nav-link" href="?page=logout">Αποσύνδεση</a></li>'; // ad logout option
            }
            ?>

          </ul> <!-- Topbar Navbar -->

        </nav> <!-- Topbar -->

        <!-- Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">

              <?php
                switch ($page){ // print the corresponding title
                case "start" :
                  echo "Καλώς ήρθατε στο Κατάστημα!";
                	break;
                case "shopinfo":
                  echo "Πληροφορίες καταστήματος";
                		break;
                case "login" :
                  echo "Login";
                	break;
                case 'products':
                  echo "Προϊόντα";
                	break;
                case 'cart':
                  echo "Καλάθι Αγορών";
                	break;
                case 'contact':
                  echo "Επικοινωνία";
                	break;
                case 'profilesettings':
                  echo "Ρυθμίσεις Προφίλ";
                	break;
                case 'do_login':
                  echo "Αποτέλεσμα Σύνδεσης";
                	break;
                case 'itemselect':
                  echo "Επιλεγμένο Προϊόν";
                	break;
                case 'addtocart':
                  echo "Προσθήκη στο καλάθι";
                	break;

                default:
                	print "Η σελίδα δεν υπάρχει";
                }// switch
              ?>
            </h1>
          </div> <!-- Page Heading -->

          <!-- MAIN CONTEND HERE -->
          <?php
          // 'require' the page chosen
          require "internal/" .$page .".php";

          ?>


        </div> <!-- Page Content -->

      </div> <!-- Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>An excercise of frustration</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->


    </div> <!-- Content Wrapper -->

  </div> <!-- Page Wrapper -->


</body>
